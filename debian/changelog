libdist-zilla-plugin-checkbin-perl (0.008-3) UNRELEASED; urgency=medium

  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Move libmodule-build-perl (back) to Build-Depends-Indep and annotate as
    with <!nocheck>, and libmodule-build-tiny-perl as well, as they are only
    needed for tests.
  * Remove unused lintian override.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Jan 2023 23:30:56 +0100

libdist-zilla-plugin-checkbin-perl (0.008-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Move libmodule-build-perl from Build-Depends-Indep to Build-Depends.
  * Move source package lintian overrides to debian/source.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libdist-zilla-perl.
    + libdist-zilla-plugin-checkbin-perl: Drop versioned constraint on
      libdist-zilla-perl in Depends.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 16:51:04 +0100

libdist-zilla-plugin-checkbin-perl (0.008-1) unstable; urgency=medium

  * Team upload

  * New upstream version 0.008
  * add libtest-needs-perl to build dependencies
  * declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Sun, 05 Nov 2017 11:13:25 +0200

libdist-zilla-plugin-checkbin-perl (0.007-1) unstable; urgency=low

  * Initial Release. (Closes: #876273)

 -- Carnë Draug <carandraug+dev@gmail.com>  Wed, 27 Sep 2017 11:46:52 +0100
